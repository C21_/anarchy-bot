const Commands = require('../commands.js');
const Config   = require('../config.js');

module.exports = {
  "action": function(bot, message, args){
    Commands.playSound(bot, message, Config.get('directories/soundboard/airhorn'));
  },
  "description": "Play an airhorn noise.",
  "permission": "commands.airhorn",
  "alias": [
    "ah"
  ]
}
