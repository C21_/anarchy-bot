const Commands = require('../commands.js');
const Config   = require('../config.js');

module.exports = {
  "action": function(bot, message, args){
    Commands.playSound(bot, message, Config.get('directories/soundboard/default') + '/taps.mp3');
  },
  "description": "They talk about my one taps.",
  "permission": "commands.taps",
  "alias": []
}
