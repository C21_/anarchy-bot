const Discord     = require('discord.js');
const path        = require('path');
const Config      = require('./config.js');
const Logger      = require('./logger.js');
const Permissions = require('./permissions.js');
const ytdl        = require('ytdl-core');
const fs          = require('fs');
const disabledCmd = Config.raw('commands/disabled');

module.exports = {
  check: function(file, user, server){
    var required = file.permission;
    if(required.indexOf('any') > -1) return true;
    if(Permissions.groupHasPermission('everyone', required)) return true;
    return Permissions.hasPermission(user, server, required);
  },
  execute(bot, message, cmd, args, file){
    if(disabledCmd.indexOf(cmd) != -1) return;
    if(this.check(file, message.sender.id, message.channel.server.id)) file.action(bot, message, args); else this.reply(bot, message, 'You lack the required permissions.', 3500);
    Logger.log('info', `(exec) ${message.sender.name} (ID: ${message.sender.id}) executed ${message.content}`);
  },
  command: function(bot, message){
    if(message.content.startsWith(Config.get('commands/prefix'))){
      var commands = this.getCommands();
      var args     = message.content.split(' ');
      var cmd      = args[0].replace('!', '');
      var parent   = this;
      commands.forEach(function(file){
        var cmdFile = require(Config.get('directories/default') + 'commands' + path.sep + file);
        if(file.replace('.js', '').toUpperCase() == cmd.toUpperCase()) parent.execute(bot, message, cmd, args, cmdFile);
        if((cmdFile.alias.length > 0) && (cmdFile.alias.indexOf(cmd) > -1)) parent.execute(bot, message, cmd, args, cmdFile);
      });
    }
  },
  getCommands: function(){
    return fs.readdirSync(Config.get('directories/default') + 'commands');
  },
  reply: function(bot, message, send, delay){
    if(delay && !isNaN(delay)){
      bot.deleteMessage(message, {wait: delay});
      bot.sendMessage(message.channel, message.sender.mention() + ' ' + send, function(err, msg){
        bot.deleteMessage(msg, {wait: delay});
      });
    } else {
      bot.sendMessage(message.channel, message.sender.mention() + ' ' + send);
    }
  },
  playSound: function(bot, message, audio, stream, vol){
    stream = stream || false;
    vol    = vol || 0.2;
    if(message.channel instanceof Discord.TextChannel){
      var server  = message.channel.server;
      var channel = (message.sender.voiceChannel != null) ? message.sender.voiceChannel : undefined;
      try{
        bot.joinVoiceChannel(channel, function(e, c){
          if(stream == "youtube"){
            c.playRawStream(ytdl(audio, {filter: 'audioonly'}), {volume: vol});
          } else if(stream == "other"){
            c.playRawStream(audio, {volume, vol});
          } else {
            if(fs.lstatSync(audio).isDirectory()){
              fs.readdir(Config.get('directories/default') + audio, function(err, file){
                var genAudioFile = function(){return Config.get('directories/default') + audio + path.sep + file[Math.floor(Math.random() * file.length)];}
                var audioFile = genAudioFile();
                while(!fs.lstatSync(audioFile).isFile()){
                  audioFile = genAudioFile();
                }
                c.playFile(audioFile, {volume: vol});
              });
            } else if(fs.lstatSync(audio).isFile()){
              c.playFile(Config.get('directories/default') + audio, {volume: vol});
            }
          }
        });
      } catch(e){
        Logger.log('info', `err (sound) => ${e}`);
      }
    }
    bot.deleteMessage(message, {wait: 3000});
  }
}
