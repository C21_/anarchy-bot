const cfgFile = require('./config/config.json');

module.exports = {
  get: function (item){
    return String(this.raw(item));
  },
  raw: function(item){
    return this.retrieve(item, cfgFile);
  },
  retrieve: function(item, file){
    var path   = item.split('/');
    var origin = file;
    var result = undefined;

    path.forEach(function(e){
      if(result == undefined)
        if(typeof origin[e] != 'undefined')
          result = origin[e];
      if(result != undefined)
        if(typeof result[e] != 'undefined')
          result = result[e];
    });
    return result;
  }
}
